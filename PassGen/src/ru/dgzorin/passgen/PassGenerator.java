package ru.dgzorin.passgen;

import java.util.Random;

public class PassGenerator {

	public String generate(String settings, int limit) {
		String result = "";
		Random random = new Random();
		for (int i = 0; i < limit; i++) {
			result += settings.charAt(random.nextInt(settings.length()));
		}

		return result;
	}
}
