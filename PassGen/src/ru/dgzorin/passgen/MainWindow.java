package ru.dgzorin.passgen;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

public class MainWindow extends JFrame {

	private final String onlyNumbers = "0123456789";
	private final String upperChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	private final String downChars = "abcdefghijklmnopqrstuvwxyz";
	private final String specialSymbols = "!@#$%^&*()|\\/+-=";

	private static final int MAIN_WINDOW_WIDTH = 420;
	private static final int MAIN_WINDOW_HEIGHT = 175;

	private final JPanel cellTwo = new JPanel(new FlowLayout());
	private final JPanel cellThree = new JPanel(new GridLayout(2, 1));
	private final JPanel cellFour = new JPanel(new FlowLayout());
	private final JPanel cellThreeUp = new JPanel(new FlowLayout());
	private final JPanel cellThreeDown = new JPanel(new GridLayout(2, 2));

	private JCheckBox bigChars = new JCheckBox("Большие", true);
	private JCheckBox smallChars = new JCheckBox("Маленькие", true);
	private JCheckBox numbers = new JCheckBox("Цифры", true);
	private JCheckBox specialСhars = new JCheckBox("Спец.символ");

	private final JLabel textPass = new JLabel("New Pa$$w0rd");
	private final JLabel textNumbSymbols = new JLabel("Количество символов: ");

	private final JTextField password = new JTextField(12);
	private final JTextField numbSymbols = new JTextField(2);

	private final JButton btnGenerate = new JButton("Создать");

	public static void main(String[] args) {
		SwingUtilities.invokeLater(MainWindow::new);
	}

	public MainWindow() {
		initGUI();
		PassGenerator generator = new PassGenerator();
		btnGenerate.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				String settings = "";
				int limit = Integer.parseInt(MainWindow.this.numbSymbols.getText());
				if (bigChars.isSelected()) {
					settings += upperChars;
				}
				if (smallChars.isSelected()) {
					settings += downChars;
				}
				if (numbers.isSelected()) {
					settings += onlyNumbers;
				}
				if (specialСhars.isSelected()) {
					settings += specialSymbols;
				}
				password.setText(generator.generate(settings, limit));
			}
		});
	}

	private void initGUI() {
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setSize(MAIN_WINDOW_WIDTH, MAIN_WINDOW_HEIGHT);
		setLocationRelativeTo(null);
		setTitle("Password Generator");
		setResizable(false);
		setLayout(new GridLayout(2, 2));

		password.setFont(new Font("Dialog", 0, 20));
		password.setEditable(false);
		password.setBackground(Color.WHITE);

		btnGenerate.setPreferredSize(new Dimension(140, 50));

		setFilter(numbSymbols, onlyNumbers, 2);
		numbSymbols.setText("8");

		cellTwo.setBackground(Color.WHITE);
		cellThree.setBackground(Color.WHITE);
		cellFour.setBackground(Color.WHITE);
		cellThreeUp.setBackground(Color.WHITE);
		bigChars.setBackground(Color.WHITE);
		smallChars.setBackground(Color.WHITE);
		numbers.setBackground(Color.WHITE);
		specialСhars.setBackground(Color.WHITE);

		cellTwo.add(textPass);
		cellTwo.add(password);
		cellThreeUp.add(textNumbSymbols);
		cellThreeUp.add(numbSymbols);
		cellThreeDown.add(bigChars);
		cellThreeDown.add(smallChars);
		cellThreeDown.add(numbers);
		cellThreeDown.add(specialСhars);
		cellThree.add(cellThreeUp);
		cellThree.add(cellThreeDown);
		cellFour.add(btnGenerate);

		add(new BgPanel());
		add(cellTwo);
		add(cellThree);
		add(cellFour);
		setVisible(true);
	}

	private static void setFilter(JTextField field, String filter, int limit) {
		field.setDocument(new PlainDocument() {
			@Override
			public void insertString(int offs, String str, AttributeSet a) throws BadLocationException {
				if (filter.indexOf(str) != -1) {
					if (getLength() < limit) {
						super.insertString(offs, str, a);
					}
				}
			}
		});
	}

}