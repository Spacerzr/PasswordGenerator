package ru.dgzorin.passgen;

import java.awt.Graphics;
import java.awt.image.BufferedImage;

import javax.swing.JPanel;

public class BgPanel extends JPanel {

	@Override
	public void paintComponent(Graphics g) {
		BufferedImage im = null;
		String path = "/progresstech.jpg";
		ImageLoader loader = new ImageLoader();
		im = loader.load(path);
		g.drawImage(im, 0, 0, null);
	}
}